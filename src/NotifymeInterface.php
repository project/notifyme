<?php

namespace Drupal\notifyme;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a notify me entity type.
 */
interface NotifymeInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the notify me title.
   *
   * @return string
   *   Title of the notify me.
   */
  public function getTitle();

  /**
   * Sets the notify me title.
   *
   * @param string $title
   *   The notify me title.
   *
   * @return \Drupal\notifyme\NotifymeInterface
   *   The called notify me entity.
   */
  public function setTitle($title);

  /**
   * Gets the notify me creation timestamp.
   *
   * @return int
   *   Creation timestamp of the notify me.
   */
  public function getCreatedTime();

  /**
   * Sets the notify me creation timestamp.
   *
   * @param int $timestamp
   *   The notify me creation timestamp.
   *
   * @return \Drupal\notifyme\NotifymeInterface
   *   The called notify me entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the notify me status.
   *
   * @return bool
   *   TRUE if the notify me is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the notify me status.
   *
   * @param bool $status
   *   TRUE to enable this notify me, FALSE to disable.
   *
   * @return \Drupal\notifyme\NotifymeInterface
   *   The called notify me entity.
   */
  public function setStatus($status);

}
